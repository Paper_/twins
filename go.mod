module gitlab.com/tslocum/twins

go 1.15

require (
	github.com/h2non/filetype v1.1.0
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/yookoala/gofast v0.4.1-0.20201013050739-975113c54107
	gitlab.com/tslocum/gmitohtml v1.0.3-0.20201203184239-2a1abe8efe7c
	golang.org/x/tools v0.0.0-20201215192005-fa10ef0b8743 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
