package main

import (
	"crypto/tls"
	"io"
	"net"
	"net/url"
	"strings"
)

func serveProxy(c net.Conn, request *url.URL, proxyURL string) int {
	tlsConfig := &tls.Config{}
	if strings.HasPrefix(proxyURL, "gemini://") {
		proxyURL = proxyURL[9:]
	} else if strings.HasPrefix(proxyURL, "gemini-insecure://") {
		proxyURL = proxyURL[18:]
		tlsConfig.InsecureSkipVerify = true
	}
	proxy, err := tls.Dial("tcp", proxyURL, tlsConfig)
	if err != nil {
		return writeStatus(c, statusProxyError)
	}
	defer proxy.Close()

	// Forward request
	proxy.Write([]byte(request.String()))
	proxy.Write([]byte(newLine))

	// Forward response
	io.Copy(c, proxy)

	return statusSuccess
}
