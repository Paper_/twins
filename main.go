package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"path"
	"syscall"
)

func init() {
	log.SetOutput(os.Stdout)
}

var quiet bool

func main() {
	log.SetFlags(0)

	configFile := flag.String("config", "", "path to configuration file")
	flag.BoolVar(&quiet, "quiet", false, "do not print access log")
	flag.Parse()

	if *configFile == "" {
		homedir, err := os.UserHomeDir()
		if err == nil && homedir != "" {
			*configFile = path.Join(homedir, ".config", "twins", "config.yaml")
		}
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP)
	go func() {
		for {
			<-sig
			err := readconfig(*configFile)
			if err != nil {
				log.Fatalf("failed to reload configuration file at %s: %v", *configFile, err)
			}
			log.Println("configuration reloaded successfully")
		}
	}()

	err := readconfig(*configFile)
	if err != nil {
		log.Fatalf("failed to read configuration file at %s: %v\nSee CONFIGURATION.md for information on configuring twins", *configFile, err)
	}

	listen(config.Listen)
}
