# twins
[![CI status](https://gitlab.com/tslocum/twins/badges/master/pipeline.svg)](https://gitlab.com/tslocum/twins/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

[Gemini](https://gemini.circumlunar.space) server

**Warning:** The twins configuration format is still under development.
Breaking changes may be made.

This page is also available at [gemini://twins.rocketnine.space](gemini://twins.rocketnine.space)

## Features

- Serve static files
  - Detect content type
  - Specify content type for files with matching extension
  - List files and directories (when enabled)
- Reverse proxy requests
  - TCP
  - [FastCGI](https://en.wikipedia.org/wiki/FastCGI)
- Serve Gemini content via HTTPS
  - Pages are converted automatically by [gmitohtml](https://gitlab.com/tslocum/gmitohtml)
- Reload configuration on `SIGHUP`

## Proposals

twins includes features that are not yet part of the Gemini specification. See [PROPOSALS.md](https://gitlab.com/tslocum/twins/blob/master/PROPOSALS.md)

## Download

twins is written in [Go](https://golang.org). Run the following command to
download and build twins from source.

```bash
go get gitlab.com/tslocum/twins
```

The resulting binary is available as `~/go/bin/twins`.

## Configure

See [CONFIGURATION.md](https://gitlab.com/tslocum/twins/blob/master/CONFIGURATION.md)

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/twins/issues).

## Dependencies

- [filetype](https://github.com/h2non/filetype) - MIME type detection
- [gofast](https://github.com/yookoala/gofast) - FastCGI client
- [go-shellquote](https://github.com/kballard/go-shellquote) - Shell string quoting
- [yaml](https://github.com/go-yaml/yaml/tree/v3) - Configuration parsing
